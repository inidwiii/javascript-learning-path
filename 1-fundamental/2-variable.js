/**
 * Variable - is a container to store a value, the value can be any type
 *
 * 3 way to declaring a variable:
 * var varname [= value] - declaring mutable variable
 * let varname [= value] - declaring mutable variable
 * const varname = value - declaring immutable variable ( can't be change after declaration )
 *                         also block scoped variable
 *
 * Different between var and let:
 * var - is a function scoping variable, it means its variable is available anywhere inside a function
 *       if variable are declared outside function, it will assigned to the global variable, declaring
 *       variable with var will let us access it even before we initializing it ( hoisting )
 * let - is a block scoping variable, block scoping is between curly braces ( {} ), if we try to access
 *       the variable outside of the curly braces ( {} ) it's will return undefined variable and
 *       'let' is inline declaration, this mean declaring variable with let wont let we access
 *       the variable before the declaration.
 */

var firstVariable
console.log(firstVariable) // 'undefined' - because we're not assigned any value

secondVariable = 10 // this is valid becase its
console.log(secondVariable) // 10 - because of variable hoisting
var secondVariable
console.log(secondVariable) // 10 - because the variable is already assigned with value

/**
 * Hoisting variable
 *
 * will assign any variable declared to the topmost or global object with undefined
 *
 * var secondVariable
 *
 * secondVariable = 10
 * console.log(secondVariable)
 * var secondVariable
 */

var thirdVariable
var thirdVariable // this is valid, we can declare it many times with 'var' keyword

console.log(thirdVariable)

let fourthValue
// let fourthValue // this is invalid, we can't declare same variable name with 'let' keyword

console.log(fourthValue)

const fifthValue = 10 // creating immutable variable that can't be change after declaration
// fifthValue = 15 // this is invalid because const is immutable

console.log(fifthValue)

function foo() {
  for (var i = 0; i < 10; i++) {
    console.log(i) // i is accessible from here
  }

  for (let j = 0; j < 10; j++) {
    console.log(j) // j is accessible from here
  }

  console.log(i) // i is accessible from here
  console.log(j) // j is un-accessible from here because of block scoping variable
}

foo()
console.log(i) // i is un-accessible from here because i is not assigned to the global object
