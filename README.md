# Modern Javascript Learning Path

Learning path to me for learning JavaScript from the start.

> **NOTE!** Please to me to follow this Learning Path for understanding the concept of JavaScript

Here the learning path:

### Fundamentals

- Running JavaScript
  - [x] Browser console
  - [x] Script tag
  - [x] Link external files
- Variables
  - [x] Declarations `var`, `let` and `const`
  - [ ] Scope
    - Global
    - Function
    - Block
  - [ ] Hoisting
- Data Types and Data Structures
  - [ ] Primitive types `undefined`, `Boolean`, `Number`, `BigInt`, `String`, and `Symbol`
  - [ ] Understanding `null` data type
  - [ ] Understanding `Object` data type
  - [ ] Understanding `Function` data type
  - [ ] Data structure types
    - [ ] Array
    - [ ] Map / Weak Map
    - [ ] Set / Weak Set
    - [ ] Date
- Type Convertions
  - [ ] Explictly convertion
  - [ ] Implicitly convertion
- Equality, understanding different of `==` with `===`
- Loops
  - [ ] Understanding `while` loop
  - [ ] Understanding `do ... while` loop
  - [ ] Understanding `for` loop
  - [ ] Understanding `break` / `continue`
  - [ ] Understanding `for ... in` loop
  - [ ] Understanding `for ... of` loop
- Control Flow
  - [ ] Understanding `if ... else`
  - [ ] Understanding `switch`
  - [ ] Understanding `try `, `catch` and `throw` ( exception )
- Expressions & Operators
  - [ ] Assignment opertators
  - [ ] Arithmetic operators
  - [ ] Logical operators
  - [ ] Conditional operators
  - [ ] Comparison operators
  - [ ] Relational operators
  - [ ] Bitwise operators
  - [ ] String operators
  - [ ] Comma operators
  - [ ] Unary operators
- Functions
  - [ ] Understanding function declarations
  - [ ] Understanding function expressions
  - [ ] Understanding calling functions
  - [ ] Understanding parameters and arguments
  - [ ] Understanding scope
  - [ ] Understanding arrow functions

### Advanced

- Advanced scope
  - [ ] Understanding lexical scoping
  - [ ] Understanding Immediately Invoked Function Expressions ( IIFE )
  - [ ] Understanding revealing module pattern
- Closure
  - [ ] Understanding closure
- Currying
  - [ ] Understanding functions currying
- `this` keyword
  - [ ] Understanding implicit binding
  - [ ] Understanding explicit binding
  - [ ] Understanding new binding
  - [ ] Understanding lexical binding
  - [ ] Understanding default binding
- Prototype
  - [ ] Understanding prototype
  - [ ] Understanding prototypal inheritance
- Class
  - [ ] Understanding ES6 Class
- Iterators & Generators
  - [ ] Understanding iterators
  - [ ] Understanding generators
- Event Loop
  - [ ] Understanding event loop ( must ) 👈
- Asynchronous JavaScript
  - [ ] Understanding `setTimeout`
  - [ ] Understanding `setInterval`
  - [ ] Understanding callbacks
  - [ ] Understanding Promises
  - [ ] Understanding `async` and `await`
- Module System
  - [ ] Understanding CommonJS ( CJS )
  - [ ] Understanding ES Module ( ESM )
  - [ ] Understanding `import` and `export`
  - [ ] Understanding default and named exports

## Web API's ( optional )

- [ ] Understanding DOM ( Document Object Model )
- [ ] Understanding XHR & fetch
- [ ] Understanding storage
- [ ] Understanding Video and Audio
- [ ] Understanding drawing graphics

## Tooling & Misc

- Linters
  - [ ] Understanding the eslint
- Code Formatters
  - [ ] Understanding prettier
- Bundlers
  - [ ] Understanding webpack / rollup / parcel
- TypeScript ( optional to learn )
